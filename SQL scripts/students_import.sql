-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 фев 2020 в 20:49
-- Версия на сървъра: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `graduation_db`
--

-- --------------------------------------------------------

--
-- Структура на таблица `studends_import`
--

CREATE TABLE `students_import` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `surname` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `fn` int(5) NOT NULL,
  `specialty` varchar(2) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `degree` enum('bachelor','masters','doctoral','') NOT NULL,
  `grade` decimal(3,2) NOT NULL,
  `username` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students_import`
--
ALTER TABLE `students_import`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fn` (`fn`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students_import`
--
ALTER TABLE `students_import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
