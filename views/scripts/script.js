(() => {

	const degreeSelect = document.getElementById('degree');
	const menuToggle = document.getElementById('toggle');

	degreeSelect && degreeSelect.addEventListener('change', event => {
		degreeSelect.style.color = "#000";
	});
	
	menuToggle && menuToggle.addEventListener('click', event => {
		var headerMenu = document.getElementById('header');
		if(headerMenu.classList.contains('menu-toggle-hide')){
			headerMenu.classList.remove('menu-toggle-hide');
		} else {
			headerMenu.classList.add('menu-toggle-hide');
		}
	});	
	
})(this);