<?php

class StatisticsModel extends BaseModel {

	public function getCountOfConfirmed() {		
		$sql = "SELECT COUNT(*) AS COUNT FROM confirmation WHERE confirmed = TRUE;";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
		
	}

	public function getCountOfAttended() {
		$sql = "SELECT COUNT(*) AS COUNT FROM attended WHERE attended = TRUE;";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getCountOfReceivedGowns() {
		$sql = "SELECT COUNT(*) AS COUNT FROM gowns WHERE recieved = true;";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getCountOfReceivedCaps() {
		$sql = "SELECT COUNT(*) AS COUNT FROM caps WHERE recieved = true;";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getCountOfAwarded() {
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE grade >= 5.5;";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getUserConfirmation() {
		$currentUser = UserController::getCurrentUsername();
		$sql = "SELECT confirmed FROM confirmation WHERE username LIKE :username;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":username", $currentUser);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC)['confirmed'];
		return $result;
	}
	
	public function getUserAttendance() {
		$currentUser = UserController::getCurrentUsername();
		$sql = "SELECT attended FROM attended WHERE username LIKE :username;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":username", $currentUser);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC)['attended'];
		return $result;
	}
		
	public function getUserReceivedGowns() {
		$currentUser = UserController::getCurrentUsername();
		$currentStudent = new StudentModel();
		$currentStudent->username = $currentUser;
		$fn = $currentStudent->getFnFromUsername();
		$sql = "SELECT recieved FROM gowns WHERE fn LIKE :fn;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":fn", $fn);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC)['recieved'];
		return $result;
	}
	
	public function getUserReceivedCaps() {
		$currentUser = UserController::getCurrentUsername();
		$currentStudent = new StudentModel();
		$currentStudent->username = $currentUser;
		$fn = $currentStudent->getFnFromUsername();
		$sql = "SELECT recieved FROM caps WHERE fn LIKE :fn;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":fn", $fn);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC)['recieved'];
		return $result;
	}
	
	
	public function getUserAwarded() {
		$currentUser = UserController::getCurrentUsername();
		$sql = "SELECT COUNT(*) AS awarded FROM students_import WHERE username LIKE :username AND grade >= 5.5;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":username", $currentUser);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC)['awarded'];
		return $result;
	}
}
?>